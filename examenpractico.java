package arbol;

/**
 *
 * @author De La Cruz Santos Alexander
 */
public class Arbol {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int dato;
        String nombre;
        ArbolBinario miArbol = new ArbolBinario();
        miArbol.agregarNodo(9, "DEMO");
        miArbol.agregarNodo(3, "DEMO");
        miArbol.agregarNodo(10, "DEMO");
        miArbol.agregarNodo(1, "DEMO");
        miArbol.agregarNodo(6, "DEMO");
        miArbol.agregarNodo(14, "DEMO");
        miArbol.agregarNodo(4, "DEMO");
        miArbol.agregarNodo(7, "DEMO");
        miArbol.agregarNodo(13, "DEMO");
        
        System.out.println("InOrden");
        if (!miArbol.estaVacio()){
            miArbol.inOrden(miArbol.raiz);
        }
        System.out.println("PreOrden");
        if (!miArbol.estaVacio()){
            miArbol.preOrden(miArbol.raiz);
        }
        System.out.println("PostOrden");
        if (!miArbol.estaVacio()){
            miArbol.postOrden(miArbol.raiz);
        }
    }
    
}

package arbol;

/**
 *
 * @author De La Cruz Santos
 */
public class ArbolBinario {

    NodoArbol raiz;

    public ArbolBinario() {
        raiz = null;
    }

    public void agregarNodo(int d, String nom) {
        NodoArbol nuevo = new NodoArbol(d, nom);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            NodoArbol auxiliar = raiz;
            NodoArbol padre;
            while (true) {
                padre = auxiliar;
                if (d < auxiliar.dato) {
                    auxiliar = auxiliar.hijoizquierdo;
                    if (auxiliar == null) {
                        padre.hijoizquierdo = nuevo;
                        return;
                    }
                } else {
                    auxiliar = auxiliar.hijoderecho;
                    if (auxiliar == null) {
                        padre.hijoderecho = nuevo;
                        return;
                    }
                }
            }
        }
    }

    public boolean estaVacio() {
        return raiz == null;
    }

    public void inOrden(NodoArbol r) {       
        if (r != null) {
            inOrden(r.hijoizquierdo);
            System.out.println(r.dato);
            inOrden(r.hijoderecho);
        }
    }

    public void preOrden(NodoArbol r) {    
        if (r != null) {
            System.out.println(r.dato);
            preOrden(r.hijoizquierdo);
            preOrden(r.hijoderecho);
        }
    }

    public void postOrden(NodoArbol r) {
        if (r != null) {          
            postOrden(r.hijoizquierdo);
            postOrden(r.hijoderecho);
            System.out.println(r.dato);
        }
    }
}
package arbol;

/**
 *
 * @author De La Cruz Santos
 */
public class NodoArbol {
    int dato;
    String nombre;
    NodoArbol hijoizquierdo, hijoderecho;
    public NodoArbol (int d, String s){
        this.dato = d;
        this.nombre = s;
        this.hijoizquierdo = null;
        this.hijoderecho = null;
    }
    public String toString(){
        return nombre + "El dato es : "+dato;
    }
}
